require 'rails_helper'

class SomeModel; end
class SomeModelPresenter < SimpleDelegator; end

RSpec.describe ApplicationHelper, :type => :helper do
  describe "#set_actived_menu" do
    it "return 'active' if params controller is the same as passed" do
      helper.params[:controller] = 'home'
      expect(helper.set_actived_menu('[home]')).to eq('active')
    end
    it "return nil if params controller is different as passed" do
      helper.params[:controller] = 'menu1'
      expect(helper.set_actived_menu('[home]')).to be_nil
    end

    it "return 'active' if params controller and action is the same as passed" do
      helper.params[:controller] = 'home'
      helper.params[:action] = 'some_action'
      expect(helper.set_actived_menu('[home]', 'some_action')).to eq('active')
    end

    it "return nil if params controller and action is different as passed" do
      helper.params[:controller] = 'home'
      helper.params[:action] = 'some_action'
      expect(helper.set_actived_menu('[home]', 'another_action')).to be_nil
    end
  end

  describe "#normalize_flash_keys" do
    it "return 'success' if key is notice" do
      expect(helper.normalize_flash_keys('notice')).to eq('success')
    end

    it "return 'error' if key is alert" do
      expect(helper.normalize_flash_keys('alert')).to eq('error')
    end

    it "return the key itself by default" do
      expect(helper.normalize_flash_keys('success')).to eq('success')
    end
  end

  describe "#present" do
    it "return a model presenter" do
      expect(helper.present(SomeModel.new)).to be_a(SomeModelPresenter)
    end
  end

  describe "#truncate_content_tag" do
    let(:description) { "Some long description. Some long description. Some long description. Some long description." }
    let(:description_short) { "Some description" }

    it "return um content_tag with attributes data-tooltip='' and title='full description')" do
      tag = helper.truncate_content_tag :span, description, 16
      expect(tag).to eq("<span title=\"Some long description. Some long description. Some long description. Some long description.\">Some long des...</span>")
    end
    it "not return tooltip if description is less than the value of truncate" do
      tag = helper.truncate_content_tag :span, description_short, 16
      expect(tag).to eq("<span>Some description</span>")
    end
  end
end
