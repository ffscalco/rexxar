require 'rails_helper'
RSpec.describe ConfirmationsController, type: :controller do
  let(:participant) {FactoryGirl.create(:participant, confirmed_at: nil)}

  it "#after_confirmation_path_for" do
    expect(controller.send("after_confirmation_path_for", "participant", participant)).to eq(new_user_session_path)
  end
end
