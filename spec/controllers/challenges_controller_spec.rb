require 'rails_helper'

RSpec.describe ChallengesController, type: :controller do
  let(:valid_attributes) {
    {
      :name => "Awesome Challenge",
      :start_date => "01/06/2016",
      :end_date => "01/06/2016"
    }
  }

  let(:invalid_attributes) {
    {
      :name => ""
    }
  }

  before(:all) do
    @user = FactoryGirl.create(:user)
  end

  before(:each) do
    sign_in(@user)
  end

  describe "GET #index" do
    it "assigns all challenges as @challenges" do
      challenge = Challenge.create! valid_attributes
      get :index, params: {}
      expect(assigns(:challenges)).to eq([challenge])
    end
  end

  describe "GET #show" do
    it "assigns the requested challenge as @challenge" do
      challenge = Challenge.create! valid_attributes
      get :show, params: {id: challenge.to_param}
      expect(assigns(:challenge)).to eq(challenge)
    end
  end

  describe "GET #new" do
    it "assigns a new challenge as @challenge" do
      get :new, params: {}
      expect(assigns(:challenge)).to be_a_new(Challenge)
    end
  end

  describe "GET #edit" do
    it "assigns the requested challenge as @challenge" do
      challenge = Challenge.create! valid_attributes
      get :edit, params: {id: challenge.to_param}
      expect(assigns(:challenge)).to eq(challenge)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Challenge" do
        expect {
          post :create, params: {challenge: valid_attributes}
        }.to change(Challenge, :count).by(1)
      end

      it "creates a new Participant" do
        valid_attributes[:participants_attributes] = [
          name: "some name",
          email: "participant_mail@email.com"
        ]

        expect {
          post :create, params: {challenge: valid_attributes}
        }.to change(Participant, :count).by(1)
      end

      it "assigns a newly created challenge as @challenge" do
        post :create, params: {challenge: valid_attributes}
        expect(assigns(:challenge)).to be_a(Challenge)
        expect(assigns(:challenge)).to be_persisted
      end

      it "redirects to the created challenge" do
        post :create, params: {challenge: valid_attributes}
        expect(response).to redirect_to(challenges_url)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved challenge as @challenge" do
        post :create, params: {challenge: invalid_attributes}
        expect(assigns(:challenge)).to be_a_new(Challenge)
      end

      it "re-renders the 'new' template" do
        post :create, params: {challenge: invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          :name => "Other challenge"
        }
      }

      it "updates the requested challenge" do
        challenge = Challenge.create! valid_attributes
        put :update, params: {id: challenge.to_param, challenge: new_attributes}
        challenge.reload
        expect(challenge.name).to eq(new_attributes[:name])
      end

      it "assigns the requested challenge as @challenge" do
        challenge = Challenge.create! valid_attributes
        put :update, params: {id: challenge.to_param, challenge: valid_attributes}
        expect(assigns(:challenge)).to eq(challenge)
      end

      it "redirects to the challenge" do
        challenge = Challenge.create! valid_attributes
        put :update, params: {id: challenge.to_param, challenge: valid_attributes}
        expect(response).to redirect_to(challenges_url)
      end
    end

    context "with invalid params" do
      it "assigns the challenge as @challenge" do
        challenge = Challenge.create! valid_attributes
        put :update, params: {id: challenge.to_param, challenge: invalid_attributes}
        expect(assigns(:challenge)).to eq(challenge)
      end

      it "re-renders the 'edit' template" do
        challenge = Challenge.create! valid_attributes
        put :update, params: {id: challenge.to_param, challenge: invalid_attributes}
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested challenge" do
      challenge = Challenge.create! valid_attributes
      expect {
        delete :destroy, params: {id: challenge.to_param}
      }.to change(Challenge, :count).by(-1)
    end

    it "redirects to the challenges list" do
      challenge = Challenge.create! valid_attributes
      delete :destroy, params: {id: challenge.to_param}
      expect(response).to redirect_to(challenges_url)
    end
  end

end
