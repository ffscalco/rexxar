require 'rails_helper'

RSpec.describe PointsController, type: :controller do
  let(:valid_attributes) {
    {
      :train => "3",
      :food_plan => "1",
      :coaching => "0",
      :sleep => "1",
      :surprise => "0",
      :punctuation_date => I18n.l(Date.today)
    }
  }

  let(:invalid_attributes) {
    {
      :train => "0",
      :food_plan => "0",
      :coaching => "0",
      :sleep => "0",
      :surprise => "0",
      :punctuation_date => I18n.l(Date.today)
    }
  }

  before(:all) do
    @user = FactoryGirl.create(:user)
  end

  before(:each) do
    sign_in(@user)
  end

  # describe "GET #index" do
  #   it "assigns all user points as @points" do
  #     point1 = FactoryGirl.build(:point, train: 3)
  #     point2 = FactoryGirl.build(:point, food_plan: 1, punctuation_date: Date.today+1.day)
  #     @user.points = [point1, point2]

  #     get :index, params: {}
  #     expect(assigns(:points)).to match_array(@user.points)
  #   end

  #   it "order for punctuation_date asc" do
  #     point1 = FactoryGirl.build(:point, train: 3, punctuation_date: Date.today-1.day)
  #     point2 = FactoryGirl.build(:point, food_plan: 1, punctuation_date: Date.today-2.day)
  #     @user.points = [point1, point2]

  #     get :index, params: {}
  #     expect(assigns(:points)).to match_array([point2, point1])
  #   end
  # end

  describe "GET #new" do
    it "assigns a new point as @point" do
      get :new, params: {}
      expect(assigns(:point)).to be_a_new(Point)
    end
  end

  describe "GET #edit" do
    it "assigns the requested point as @point" do
      point = create_point

      get :edit, params: {id: point.to_param}
      expect(assigns(:point)).to eq(point)
    end
  end

  describe "POST #create" do
    let!(:challenge) { FactoryGirl.create(:challenge) }

    context "with valid params" do
      it "creates a new Point" do
        expect {
          post :create, params: {point: valid_attributes}
        }.to change(Point, :count).by(1)
      end

      it "assigns a newly created point as @point" do
        post :create, params: {point: valid_attributes}
        expect(assigns(:point)).to be_a(Point)
        expect(assigns(:point)).to be_persisted
      end

      it "redirects to dashboard" do
        post :create, params: {point: valid_attributes}
        expect(response).to redirect_to(dashboard_index_url)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved point as @point" do
        post :create, params: {point: invalid_attributes}
        expect(assigns(:point)).to be_a_new(Point)
      end

      it "re-renders the 'new' template" do
        post :create, params: {point: invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          :coaching => "1"
        }
      }

      it "updates the requested challenge" do
        point = create_point
        put :update, params: {id: point.to_param, point: new_attributes}
        point.reload
        expect(point.coaching).to eq(1)
      end

      it "assigns the requested point as @point" do
        point = create_point
        put :update, params: {id: point.to_param, point: valid_attributes}
        expect(assigns(:point)).to eq(point)
      end

      it "redirects to the points index" do
        point = create_point
        put :update, params: {id: point.to_param, point: valid_attributes}
        expect(response).to redirect_to(dashboard_index_url)
      end
    end

    context "with invalid params" do
      it "assigns the point as @point" do
        point = create_point
        put :update, params: {id: point.to_param, point: invalid_attributes}
        expect(assigns(:point)).to eq(point)
      end

      it "re-renders the 'edit' template" do
        point = create_point
        put :update, params: {id: point.to_param, point: invalid_attributes}
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested point" do
      point = create_point
      expect {
        delete :destroy, params: {id: point.to_param}
      }.to change(Point, :count).by(-1)
    end

    it "redirects to the points list" do
      point = create_point
      delete :destroy, params: {id: point.to_param}
      expect(response).to redirect_to(dashboard_index_url)
    end
  end

  def create_point
    challenge = FactoryGirl.create(:challenge)
    point = Point.new valid_attributes
    point.user = @user
    point.challenge = challenge
    point.save

    point
  end

end
