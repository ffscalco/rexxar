require 'rails_helper'

class AuthenticationController < ApplicationController; end
class Authentication < ActiveRecord::Base; end

RSpec.describe AuthenticationController, type: :controller do
  controller do
    def index
      head :ok
    end
    def new
      head :ok
    end
    def create
      head :ok
    end
    def edit
      head :ok
    end
    def update
      head :ok
    end
    def destroy
      head :ok
    end
  end

  context "When not logged in" do
    it "redirect to new user session when try to access index" do
      process :index, method: :get, params:{}
      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "redirect to new user session when try to access new" do
      process :new, method: :get
      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "redirect to new user session when try to access edit" do
      process :update, method: :get, params: {:id => 1}
      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "redirect to new user session when try to access create" do
      process :create, method: :post, params: {some: :attribute}
      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "redirect to new user session when try to access update" do
      process :update, method: :put, params: {:id => 1, some: :attribute}

      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "redirect to new user session when try to access destroy" do
      process :destroy, method: :delete, params: {:id => 1}

      expect(response).to redirect_to(new_user_session_path(locale: ''))
    end

    it "use login layout" do
      expect(controller.send(:set_layout)).to eq("login")
    end
  end

  context "When logged in" do
    let(:user) {FactoryGirl.create(:user)}

    before do
      sign_in(user)
    end

    it "access index" do
      process :index, method: :get
      expect(response.code).to eq("200")
    end

    it "access new" do
      process :new, method: :get
      expect(response.code).to eq("200")
    end

    it "access edit" do
      process :edit, method: :get, params: {:id => 1}
      expect(response.code).to eq("200")
    end

    it "access create" do
      process :create, method: :post, params: {some: :attribute}
      expect(response.code).to eq("200")
    end

    it "access update" do
      process :update, method: :put, params: {:id => 1, some: :attribute}

      expect(response.code).to eq("200")
    end

    it "access destroy" do
      process :destroy, method: :delete, params: {:id => 1}

      expect(response.code).to eq("200")
    end

    it "use login layout" do
      expect(controller.send(:set_layout)).to eq("application")
    end
  end
end
