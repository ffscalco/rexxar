require 'rails_helper'
RSpec.describe SessionsController, type: :controller do
  let(:user) {FactoryGirl.create(:user)}

  describe "#after_sign_in_path_for" do
    it "redirect to edit_user_registration_path if is the first login" do
      user.update_attribute(:sign_in_count, 1)
      expect(controller.send("after_sign_in_path_for", user)).to eq(edit_user_registration_path)
    end

    it "redirect to root_path if isn't the first login" do
      user.update_attribute(:sign_in_count, 2)
      expect(controller.send("after_sign_in_path_for", user)).to eq(root_path)
    end
  end
end
