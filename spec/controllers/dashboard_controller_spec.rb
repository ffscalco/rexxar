require 'rails_helper'
RSpec.describe DashboardController, type: :controller do
  let(:user) {FactoryGirl.create(:user)}

  it "access index" do
    sign_in(user)

    process :index, method: :get
    expect(response.code).to eq("200")
  end
end
