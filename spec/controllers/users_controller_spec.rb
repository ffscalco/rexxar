require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before(:all) do
    @user = FactoryGirl.create(:user)
  end

  before(:each) do
    sign_in(@user)
  end

  describe "PUT #update_levelup" do
    context "when success" do
      it "return json with status 200 if is updated" do
        put :update_levelup, xhr: true

        expect(response.status).to eq(200)
      end
      it "update levelup attribute" do
        @user.update_attribute(:levelup, true)
        put :update_levelup, xhr: true

        @user.reload
        expect(@user.levelup).to eq(false)
      end
    end

    context "when failure" do
      before(:each) do
        allow_any_instance_of(User).to receive(:update) { false }
      end

      it "return json with status 422 if isn't updated" do
        put :update_levelup, xhr: true

        expect(response.status).to eq(422)
      end

      it "not update levelup attribute" do
        @user.update_attribute(:levelup, true)
        put :update_levelup, xhr: true

        @user.reload
        expect(@user.levelup).to eq(true)
      end
    end

  end
end
