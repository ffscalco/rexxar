FactoryGirl.define do
  factory :participant do
    sequence(:email) { |n| "some_participant+#{n}@email.com" }
    password "123456"
    name "Some name"
    confirmed_at Date.today
    admin false
  end
end
