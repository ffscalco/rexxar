FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "some+#{n}@email.com" }
    password "123456"
    name "Some name"
    admin true
  end
end
