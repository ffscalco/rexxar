FactoryGirl.define do
  factory :challenge do
    name "MyString"
    start_date Date.today
    end_date Date.today+30.days
  end
end
