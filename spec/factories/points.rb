FactoryGirl.define do
  factory :point do
    user { FactoryGirl.build(:user) }
    challenge { FactoryGirl.build(:challenge, start_date: Date.today-15.days, end_date: Date.today+15.days) }
    punctuation_date Date.today
    food_plan 1
  end
end
