require 'rails_helper'

RSpec.describe ChallengePresenter, type: :presenter do
  context "formatations" do
    describe "start date" do
      it "translate start date if exists" do
        date = Date.today
        presenter = ChallengePresenter.new(FactoryGirl.build(:challenge, start_date: date))

        expect(presenter.format_start_date).to eq("#{date.day.to_s.rjust(2, "0")}/#{date.month.to_s.rjust(2, "0")}/#{date.year}")
      end

      it "return blank if start date is nil" do
        presenter = ChallengePresenter.new(FactoryGirl.build(:challenge, start_date: nil))

        expect(presenter.format_start_date).to eq(nil)
      end
    end

    describe "end date" do
      it "translate end date if exists" do
        date = Date.today
        presenter = ChallengePresenter.new(FactoryGirl.build(:challenge, end_date: date))

        expect(presenter.format_end_date).to eq("#{date.day.to_s.rjust(2, "0")}/#{date.month.to_s.rjust(2, "0")}/#{date.year}")
      end

      it "return blank if end date is nil" do
        presenter = ChallengePresenter.new(FactoryGirl.build(:challenge, end_date: nil))

        expect(presenter.format_end_date).to eq(nil)
      end
    end
  end
end
