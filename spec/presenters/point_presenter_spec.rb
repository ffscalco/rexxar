require 'rails_helper'

RSpec.describe PointPresenter, type: :presenter do
  context "formatations" do
    describe "punctuation_date" do
      it "translate punctuation" do
        date = Date.today
        presenter = PointPresenter.new(FactoryGirl.build(:point, punctuation_date: date))

        expect(presenter.format_punctuation_date).to eq("#{date.day.to_s.rjust(2, "0")}/#{date.month.to_s.rjust(2, "0")}/#{date.year}")
      end

      it "set today date if punctuation date is nil" do
        presenter = PointPresenter.new(FactoryGirl.build(:point, punctuation_date: nil))

        date = Date.today
        expect(presenter.format_punctuation_date).to eq("#{date.day.to_s.rjust(2, "0")}/#{date.month.to_s.rjust(2, "0")}/#{date.year}")
      end
    end
  end

  context "#format_rank" do
    let(:presenter) { PointPresenter.new(FactoryGirl.build(:point)) }
    let(:prng) {Random.new}

    it "return 'Moisés'" do
      expect(presenter.format_rank(prng.rand(0..14))).to eq([I18n.t('activerecord.attributes.user.ranks.moises'), "#BF7ADC"])
    end
    it "return 'Estagiário'" do
      expect(presenter.format_rank(prng.rand(15..29))).to eq([I18n.t('activerecord.attributes.user.ranks.trainee'), "#E05142"])
    end
    it "return 'Adaptado'" do
      expect(presenter.format_rank(prng.rand(30..44))).to eq([I18n.t('activerecord.attributes.user.ranks.adapted'), "#FFAE36"])
    end
    it "return 'Scaled'" do
      expect(presenter.format_rank(prng.rand(45..59))).to eq([I18n.t('activerecord.attributes.user.ranks.scaled'), "#20D4C9"])
    end
    it "return 'RX'" do
      expect(presenter.format_rank(prng.rand(60..89))).to eq([I18n.t('activerecord.attributes.user.ranks.rx'), "#3DA3F3"])
    end
    it "return 'Mito'" do
      expect(presenter.format_rank(prng.rand(90..119))).to eq([I18n.t('activerecord.attributes.user.ranks.myth'), "#D3793D"])
    end
    it "return 'Hero'" do
      expect(presenter.format_rank(prng.rand(120..160))).to eq([I18n.t('activerecord.attributes.user.ranks.hero'), "#A5B6B7"])
    end
  end

end
