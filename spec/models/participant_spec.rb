require 'rails_helper'

RSpec.describe Participant, type: :model do
  describe "validations" do
    it "require a name" do
      is_expected.to validate_presence_of(:name)
    end

    it "has and belongs to many challenges" do
      is_expected.to have_and_belong_to_many(:challenges)
    end
  end

  context "#admin?" do
    it "return false" do
      participant = FactoryGirl.build(:participant)
      expect(participant.admin?).to eq(false)
    end
  end
end
