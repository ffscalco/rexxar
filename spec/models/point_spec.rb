require 'rails_helper'

RSpec.describe Point, type: :model do
  describe "validations" do
    it "require an user" do
      is_expected.to validate_presence_of(:user)
    end

    it "require a challenge" do
      is_expected.to validate_presence_of(:challenge)
    end

    it "require a punctuation_date" do
      is_expected.to validate_presence_of(:punctuation_date)
    end

    it "belong_to user" do
      is_expected.to belong_to(:user)
    end

    it "belong_to challenge" do
      is_expected.to belong_to(:challenge)
    end

    it "require some points" do
      point = FactoryGirl.build(:point, food_plan: 0)
      expect(point).to be_invalid
      expect(point.errors[:base]).to match_array([I18n.t('activerecord.errors.models.point.no_points')])
    end

    it "punctuation_date must be between challenge star date and end date" do
      point = FactoryGirl.build(:point, punctuation_date: Date.today+20.days)
      expect(point).to be_invalid
      expect(point.errors[:punctuation_date]).to match_array(["#{I18n.t('activerecord.errors.models.point.out_of_range')} (#{I18n.l(point.challenge.start_date)} - #{I18n.l(point.challenge.end_date)})"])
    end

    it "punctuation_date must be unique" do
      allow_any_instance_of(Point).to receive(:check_user_rank).and_return(true)

      is_expected.to validate_uniqueness_of(:punctuation_date).scoped_to(:user_id).with_message(I18n.t('activerecord.errors.models.point.punctuation_date'))
    end
  end

  describe "#sum_points" do
    it "sum total points" do
      point = FactoryGirl.build(:point, train: 3, sleep: 1, coaching: 1, food_plan: 1, surprise: 2)
      expect(point.sum_points).to eq(8)
    end
  end

  describe "#check_user_rank" do
    let(:user) { FactoryGirl.create(:user) }

    it "not update user if don't change rank" do
      FactoryGirl.create(:point, train: 3, punctuation_date: Date.today-1.day, user: user)

      expect(user.rank).to eq("moises")
      expect(user.levelup).to eq(false)
    end

    it "update only rank if new rank is lower than actual rank" do
      user.update_attribute(:rank, 5)
      FactoryGirl.create(:point, train: 30, punctuation_date: Date.today-1.day, user: user)

      expect(user.rank).to eq("adapted")
      expect(user.levelup).to eq(false)
    end

    it "update user if change rank" do
      FactoryGirl.create(:point, train: 30, punctuation_date: Date.today-1.day, user: user)

      expect(user.rank).to eq("adapted")
      expect(user.levelup).to eq(true)
    end
  end

  describe "#ranks" do
    let(:prng) {Random.new}
    let(:point) {FactoryGirl.build(:point)}

    it "return 0" do
      expect(point.send(:ranks, prng.rand(0..14))).to eq(0)
    end
    it "return 1" do
      expect(point.send(:ranks, prng.rand(15..29))).to eq(1)
    end
    it "return 2" do
      expect(point.send(:ranks, prng.rand(30..44))).to eq(2)
    end
    it "return 3" do
      expect(point.send(:ranks, prng.rand(45..59))).to eq(3)
    end
    it "return 4" do
      expect(point.send(:ranks, prng.rand(60..89))).to eq(4)
    end
    it "return 5" do
      expect(point.send(:ranks, prng.rand(90..119))).to eq(5)
    end
    it "return 6" do
      expect(point.send(:ranks, prng.rand(120..160))).to eq(6)
    end
  end
end
