require 'rails_helper'

RSpec.describe Challenge, type: :model do
  describe "validations" do
    it "require a name" do
      is_expected.to validate_presence_of(:name)
    end

    it "require a start date" do
      is_expected.to validate_presence_of(:start_date)
    end

    it "require a end date" do
      is_expected.to validate_presence_of(:end_date)
    end

    it "has and belongs to many participants" do
      is_expected.to have_and_belong_to_many(:participants)
    end

    it "accept nested attributes for participants" do
      is_expected.to accept_nested_attributes_for(:participants)
    end
  end
end
