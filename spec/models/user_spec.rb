require 'rails_helper'

RSpec.describe User, type: :model do
  describe "validations" do
    it "require a name" do
      is_expected.to validate_presence_of(:name)
    end

    it "rank must be an enum" do
      is_expected.to define_enum_for(:rank)
    end
  end

  context "#admin?" do
    it "return true" do
      user = FactoryGirl.build(:user)
      expect(user.admin?).to eq(true)
    end
  end

  context "#total_points" do
    it "return user total points" do
      user = FactoryGirl.build(:user)
      point = FactoryGirl.build(:point, train: 3, food_plan: 1, sleep: 1)
      point2 = FactoryGirl.build(:point, surprise: 2, punctuation_date: Date.today+1.day)

      user.points = [point, point2]

      expect(user.total_points).to eq(8)
    end
  end

  context "#update_rank" do
    let(:user) { FactoryGirl.create(:user) }

    it "update rank" do
      expect(user.rank).to eq("moises")

      user.update_rank(2)
      user.reload

      expect(user.rank).to eq("adapted")
    end

    it "update levelup" do
      expect(user.levelup).to eq(false)

      user.update_rank(2)
      user.reload

      expect(user.levelup).to eq(true)
    end
  end
end
