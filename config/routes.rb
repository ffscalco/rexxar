Rails.application.routes.draw do
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    resources :challenges
    resources :points, except: [:show, :index]
    root :to => "dashboard#index"

    devise_for :users, controllers: { sessions: "sessions" }
    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put 'users' => 'devise/registrations#update', :as => 'user_registration'
      put 'users/update_levelup' => 'users#update_levelup', :as => 'user_update_levelup', :defaults =>{:format => :js}
    end
    devise_for :participants, only: [:confirmations], controllers: { confirmations: 'confirmations' }
    resources :dashboard, only: [:index]
  end

  get '*path', to: redirect("/#{I18n.default_locale}/%{path}"), constraints: lambda { |req| !req.path.starts_with? "/#{I18n.default_locale}/" }
  get '', to: redirect("/#{I18n.default_locale}")
end
