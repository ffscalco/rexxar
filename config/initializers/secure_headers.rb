::SecureHeaders::Configuration.configure do |config|
  config.hsts = "max-age=#{1.week.to_i}; includeSubdomains; preload"
  config.x_frame_options = 'DENY'
  config.x_content_type_options = "nosniff"
  config.x_xss_protection = "1; mode=block"
  config.csp = {
    # report_only: Rails.env.production?, # for the Content-Security-Policy-Report-Only header
    preserve_schemes: false, # default: false.
    font_src: %w('self' data: fonts.gstatic.com https://fonts.gstatic.com),
    default_src: %w('self' data:),
    base_uri: %w('self'),
    script_src: %w('self' 'unsafe-inline'), # scripts only allowed in external files from the same origin
    style_src: %w('self' 'unsafe-inline'), # styles only allowed in external files from the same origin and in style attributes (for now)
  }
end
