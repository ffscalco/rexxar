class AddRankToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :rank, :integer, default: 0, null: false
    add_column :users, :levelup, :boolean, default: false, null: false
  end
end
