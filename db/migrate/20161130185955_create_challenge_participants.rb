class CreateChallengeParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :challenges_users do |t|
      t.integer :participant_id, foreign_key: true
      t.integer :challenge_id, foreign_key: true
    end
  end
end
