class CreatePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :points do |t|
      t.references :user, foreign_key: true
      t.references :challenge, foreign_key: true
      t.integer :train, default: 0
      t.integer :food_plan, default: 0
      t.integer :coaching, default: 0
      t.integer :sleep, default: 0
      t.integer :surprise, default: 0
      t.date :punctuation_date
      t.string :obs

      t.timestamps
    end
  end
end
