source 'https://rubygems.org'
ruby '2.4.1'

gem 'rails',                  '5.1.4'
gem 'puma',                   '3.11.2'
gem 'secure_headers',         '5.0.5'
gem 'jquery-rails',           '4.3.1'
gem 'jquery-turbolinks',      '2.1.0'
gem 'therubyracer',           '0.12.3'
gem 'less-rails',             '3.0.0'
gem 'font-awesome-rails',     '4.7.0.3'
gem 'responders',             '2.4.0'
gem 'turbolinks',             '5.1.0'
gem 'slim-rails',             '3.1.3'
gem 'pg',                     '0.19.0'
gem 'uglifier',               '3.0.3'
gem 'simple_form',            '3.5.0'
gem 'flutie',                 '2.0.0'
gem 'rollbar',                '2.13.3'
gem 'devise',                 '4.4.1'
gem 'cocoon',                 '1.2.9'
gem 'i18n-js',                '3.0.1'

group :production, :staging do
  gem 'rails_12factor',       '0.0.3'
  gem 'rack-canonical-host',  '0.2.2'
  gem 'newrelic_rpm',         '~> 3.12'
  gem 'librato-rails',        '2.1.0'
end

group :development do
  gem 'foreman',              '0.82.0'
  gem 'jumpup',               '0.0.8'
  gem 'jumpup-heroku',        '0.0.6'
  gem 'better_errors',        '2.1.1'
  gem 'binding_of_caller',    '0.7.2'
  gem 'letter_opener',        '1.4.1'
  gem 'bullet',               '5.4.2'
  gem 'listen',               '3.1.5'
end

group :test do
  gem 'shoulda-matchers',     '3.1.1', require: false
  gem 'simplecov',            '0.12.0', require: false
  gem 'email_spec',           '2.1.0'
  gem 'database_cleaner',     '1.5.3'
end

group :development, :test do
  gem 'rspec-rails',              '3.7.2'
  gem 'factory_girl_rails',       '4.9.0'
  gem 'pry-rails',                '0.3.4'
  gem 'dotenv-rails',             '2.2.1'
  gem 'awesome_print',            '1.7.0'
  gem 'spring-commands-rspec',    '1.0.4'
  gem 'byebug',                   '9.0.6'
  gem 'spring',                   '2.0.0'
  gem 'fuubar',                   '2.2.0'
  gem 'rails-controller-testing', '1.0.2'
end
