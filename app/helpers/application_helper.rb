module ApplicationHelper
  def set_actived_menu(controllers, action=nil)
    active = nil
    action.nil? ? (active = 'active' unless !controllers.include?(params[:controller])) :
      (active = 'active' if controllers.include?(params[:controller]) and action.include?(params[:action]))

    return active
  end

  def present(model)
    klass = "#{model.class}Presenter".constantize
    klass.new(model)
  end

  def normalize_flash_keys(key)
    case key
    when "notice"
      key = "success"
    when "alert"
      key = "error"
    else
      key
    end
  end

  def truncate_content_tag(tag, description, truncate)
    return "" if description.blank?
    options = {:title=>description } if description.length >truncate
    content_tag(tag, description.truncate(truncate), options)
  end
end
