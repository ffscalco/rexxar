class UsersController < ApplicationController
  def update_levelup
    return render json: {success: true}, status: 200 if current_user.update({levelup: false})

    return render json: {error: true}, status: 422
  end
end
