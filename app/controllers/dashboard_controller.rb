class DashboardController < ApplicationController
  def index
    @point = Point.new
    @points = current_user.points.all.order(:punctuation_date)
  end
end
