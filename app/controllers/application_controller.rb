require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder

  protect_from_forgery with: :exception

  respond_to :html

  around_action :with_locale
  before_action :authenticate_user!

  layout :set_layout

  private
    def set_layout
      user_signed_in? ? 'application' : 'login'
    end

    def with_locale
      I18n.with_locale(params[:locale]) { yield }
    end

    def default_url_options(options = {})
      { locale: I18n.locale }
    end
end
