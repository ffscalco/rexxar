class PointsController < ApplicationController
  before_action :set_challenge, only: [:edit, :update, :destroy]

  # GET /points
  # def index
  #   @points = current_user.points.all.order(:punctuation_date)
  #   respond_with(@points)
  # end

  # GET /points/new
  def new
    @point = Point.new
    respond_with(@point)
  end

  # GET /points/1/edit
  def edit
  end

  # POST /points
  def create
    @point = Point.new(point_params)

    @point.user = current_user
    @point.challenge = Challenge.first

    @point.save

    respond_with(@point, location: dashboard_index_path)
  end

  # PATCH/PUT /points/1
  def update
    @point.update(point_params)
    respond_with(@point, location: dashboard_index_path)
  end

  # DELETE /points/1
  def destroy
    @point.destroy
    respond_with(@point, location: dashboard_index_path)
  end

  private
    def set_challenge
      @point = Point.find(params[:id])
    end

    def point_params
      params.require(:point).permit(:punctuation_date, :train, :food_plan, :coaching, :sleep, :surprise, :obs)
    end
end
