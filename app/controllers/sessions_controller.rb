class SessionsController < Devise::SessionsController
  self.responder = ActionController::Responder

  def after_sign_in_path_for(resource)
    return edit_user_registration_path if resource.sign_in_count == 1

    root_path
  end
end
