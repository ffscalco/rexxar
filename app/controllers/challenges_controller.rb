class ChallengesController < ApplicationController
  before_action :set_challenge, only: [:show, :edit, :update, :destroy]

  # GET /challenges
  def index
    @challenges = Challenge.all
    respond_with(@challenges)
  end

  # GET /challenges/1
  def show
  end

  # GET /challenges/new
  def new
    @challenge = Challenge.new
    respond_with(@challenge)
  end

  # GET /challenges/1/edit
  def edit
  end

  # POST /challenges
  def create
    @challenge = Challenge.new(challenge_params)

    @challenge.save
    respond_with(@challenge, location: challenges_path)
  end

  # PATCH/PUT /challenges/1
  def update
    @challenge.update(challenge_params)
    respond_with(@challenge, location: challenges_path)
  end

  # DELETE /challenges/1
  def destroy
    @challenge.destroy
    respond_with(@challenge)
  end

  private
    def set_challenge
      @challenge = Challenge.includes(:participants).find(params[:id])
    end

    def challenge_params
      params.require(:challenge).permit(:name, :start_date, :end_date, participants_attributes: [:id, :name, :email, :_destroy])
    end
end
