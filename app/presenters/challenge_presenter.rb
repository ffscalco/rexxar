class ChallengePresenter < SimpleDelegator
  attr_reader :challenge

  def initialize(challenge)
    @challenge = challenge
    __setobj__(challenge)
  end

  def format_start_date
    I18n.l(challenge.start_date) unless challenge.start_date.nil?
  end

  def format_end_date
    I18n.l(challenge.end_date) unless challenge.end_date.nil?
  end
end
