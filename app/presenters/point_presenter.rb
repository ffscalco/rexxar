class PointPresenter < SimpleDelegator
  attr_reader :point

  def initialize(point)
    @point = point
    __setobj__(point)
  end

  def format_punctuation_date
    point.punctuation_date = Date.today if point.punctuation_date.nil?
    I18n.l(point.punctuation_date)
  end

  def format_rank(points)
    case points
    when 0..14
      return [I18n.t("activerecord.attributes.user.ranks.moises"), "#BF7ADC"]
    when 15..29
      return [I18n.t("activerecord.attributes.user.ranks.trainee"), "#E05142"]
    when 30..44
      return [I18n.t("activerecord.attributes.user.ranks.adapted"), "#FFAE36"]
    when 45..59
      return [I18n.t("activerecord.attributes.user.ranks.scaled"), "#20D4C9"]
    when 60..89
      return [I18n.t("activerecord.attributes.user.ranks.rx"), "#3DA3F3"]
    when 90..119
      return [I18n.t("activerecord.attributes.user.ranks.myth"), "#D3793D"]
    when 120..160
      return [I18n.t("activerecord.attributes.user.ranks.hero"), "#A5B6B7"]
    end
  end
end
