$(document).on('turbolinks:load', function() {
  if ($("#morris-donut-graph-rank").length > 0) {
    Morris.Donut({
      element: 'morris-donut-graph-rank',
      data: [
        {value: 160, label: I18n.t('activerecord.attributes.user.ranks.hero'), formatted: '120 ' + I18n.t('activerecord.models.point', {count: 0})},
        {value: 119, label: I18n.t('activerecord.attributes.user.ranks.myth'), formatted: '90 ~ 119 ' + I18n.t('activerecord.models.point', {count: 2})},
        {value: 89, label: I18n.t('activerecord.attributes.user.ranks.rx'), formatted: '60 ~ 89 ' + I18n.t('activerecord.models.point', {count: 2})},
        {value: 59, label: I18n.t('activerecord.attributes.user.ranks.scaled'), formatted: '45 ~ 59 ' + I18n.t('activerecord.models.point', {count: 2})},
        {value: 44, label: I18n.t('activerecord.attributes.user.ranks.adapted'), formatted: '30 ~ 44 ' + I18n.t('activerecord.models.point', {count: 2})},
        {value: 29, label: I18n.t('activerecord.attributes.user.ranks.trainee'), formatted: '15 ~ 29 ' + I18n.t('activerecord.models.point', {count: 2})},
        {value: 14, label: I18n.t('activerecord.attributes.user.ranks.moises'), formatted: '0 ~ 14 ' + I18n.t('activerecord.models.point', {count: 2})}
      ],
      colors: $('#morris-donut-graph-rank').data('colors').split(','),
      formatter: function (x, data) {
        return data.formatted;
      }
    });
  }
});
