// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require i18n/translations
//= require jquery2
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks

//= require js/libs/jquery-ui/jquery-ui.min
//= require js/libs/bootstrap/bootstrap.min

//= require js/libs/spin.js/spin.min
//= require js/libs/autosize/jquery.autosize.min
//= require js/libs/nanoscroller/jquery.nanoscroller.min

//= require js/libs/toastr/toastr
//= require toastr.config

//= require js/libs/select2/select2.min
//= require js/libs/select2/select2_locale_pt-BR
//= require select2

//= require breadcrumbs

//= require js/libs/bootstrap-datepicker/bootstrap-datepicker
//= require js/libs/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR
//= require js/libs/inputmask/jquery.inputmask.bundle.min
//= require js/libs/raphael/raphael-min
//= require js/libs/morris.js/morris.min
//= require js/libs/jquery-knob/jquery.knob.min
//= require bootstrap-tour/bootstrap-tour.min
//= require knob_chart
//= require morris_chart
//= require inputmask
//= require datetime.config

//= require js/core/source/App
//= require js/core/source/AppCard
//= require js/core/source/AppForm
//= require js/core/source/AppNavigation
//= require js/core/source/AppVendor

//= require cocoon

//= require models/points
//= require congratz_message

//= require_self
