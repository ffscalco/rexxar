$(document).on('turbolinks:load', function() {
  if ($("#congratzModal").length > 0) {
    $("#congratzModal").modal();

    $.ajax({
      url: $("#congratzModal").attr("data-url"),
      method: "PUT",
      success: function(data) {
      },
      error: function(xhr) {
        var errors = $.parseJSON(xhr.responseText).error;
        console.log(errors);
      }
    });
  }
});
