$(document).on('turbolinks:load', function() {
    $("input#points_surprise_challenge").on("click", function() {
        $("div.surprise-points").slideToggle();
    })

    if ($("#point_surprise").val() !== "0") {
      $("input#points_surprise_challenge").trigger("click");
    }
});
