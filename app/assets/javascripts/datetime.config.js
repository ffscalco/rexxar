$(document).on('turbolinks:load', function() {
  $('input.date').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy", language: I18n.currentLocale()});
  $('input.time-mask').inputmask('h:s', {placeholder: 'hh:mm', showMaskOnHover: false});
});
