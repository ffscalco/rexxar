$(document).on('turbolinks:load', function() {
  if ($("form#edit_user").length > 0) {
    var tour = new Tour({
      name: "change-password",
      steps: [
        {
          element: ".tour-one",
          title: "Welcome!",
          content: "Let's go for a little tutorial.",
          placement: "bottom"
        },
        {
          element: ".tour-one",
          title: "Changing the password",
          content: "This is the password change page, we have redirected you here on your first login to facilitate ;)",
          placement: "bottom"
        },
        {
          title: "Changing the password",
          element: "ul.header-nav-profile",
          content: "To access this page later, just click here, and then on the \"Change Password\" link.",
          placement: "bottom"
        },
        {
          title: "Exiting the app",
          element: "ul.header-nav-profile",
          content: "In this same place, you have the option to exit the application, but do not quit now! Hehehe",
          placement: "bottom"
        },
        {
          title: "Dashboard",
          element: "a.btn.btn-raised.btn-info.ink-reaction",
          content: "After you have decide to change the password, click here to see your dashboard",
          placement: "bottom"
        }
      ],
      template: "<div class='card popover tour'>" +
                  "<div class='arrow'></div>" +
                  "<div class='card-head card-head-xs style-primary'>" +
                    "<header class='popover-title'></header>" +
                  "</div>" +
                  "<div class='card-body popover-content'>" +
                  "</div>" +
                  "<div class='popover-navigation'>" +
                    "<button class='btn btn-info' data-role='prev'>« " + I18n.t('actions.previous') + "</button>" +
                    "<span data-role='separator'>     </span>" +
                    "<button class='btn btn-info' data-role='next'>" + I18n.t('actions.next') + " »</button>" +
                    "<button class='btn btn-success' data-role='end'>" + I18n.t('actions.end') + "</button>" +
                  "</div>" +
                "</div>",
      onEnd: function (tour) {
        $(".tour-change-password").remove();
      }
    });

    tour.init();
    tour.start();
  }
});
