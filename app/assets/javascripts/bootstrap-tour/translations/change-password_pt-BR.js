$(document).on('turbolinks:load', function() {
  if ($("form#edit_user").length > 0) {
    var tour = new Tour({
      name: "change-password",
      steps: [
        {
          element: ".tour-one",
          title: "Seja bem vindo!",
          content: "Vamos para um pequeno tutorial.",
          placement: "bottom"
        },
        {
          element: ".tour-one",
          title: "Trocando a senha",
          content: "Esta é a tela para trocar de senha, já redirecionamos você aqui no seu primeiro login para facilitar ;)",
          placement: "bottom"
        },
        {
          title: "Trocando a senha",
          element: "ul.header-nav-profile",
          content: "Para acessar essa tela mais tarde é só clicar aqui, e em seguida no link \"Alterar Senha\"",
          placement: "bottom"
        },
        {
          title: "Saindo do aplicativo",
          element: "ul.header-nav-profile",
          content: "Neste mesmo lugar, está a opção para sair do aplicativo, mas não saia agora! hehehe",
          placement: "bottom"
        },
        {
          title: "Dashboard",
          element: "a.btn.btn-raised.btn-info.ink-reaction",
          content: "Depois que você decidir trocar a senha, de um clique aqui para conhecer a sua dashboard",
          placement: "bottom"
        }
      ],
      template: "<div class='card popover tour'>" +
                  "<div class='arrow'></div>" +
                  "<div class='card-head card-head-xs style-primary'>" +
                    "<header class='popover-title'></header>" +
                  "</div>" +
                  "<div class='card-body popover-content'>" +
                  "</div>" +
                  "<div class='popover-navigation'>" +
                    "<button class='btn btn-info' data-role='prev'>« " + I18n.t('actions.previous') + "</button>" +
                    "<span data-role='separator'>     </span>" +
                    "<button class='btn btn-info' data-role='next'>" + I18n.t('actions.next') + " »</button>" +
                    "<button class='btn btn-success' data-role='end'>" + I18n.t('actions.end') + "</button>" +
                  "</div>" +
                "</div>",
      onEnd: function (tour) {
        $(".tour-change-password").remove();
      }
    });

    tour.init();
    tour.start();
  }
});
