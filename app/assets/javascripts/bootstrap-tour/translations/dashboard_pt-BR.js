$(document).on('turbolinks:load', function() {
  if ($("div.general-rank").length > 0) {

    var tour = new Tour({
      name: "dashboard",
      steps: [
        {
          element: "div.rank",
          title: "Rank do desafio",
          content: "Muito bem! Este é o rank do desafio, quanto maior for sua pontuação, maior será seu rank. Todos começam como Moisés, mas apenas alguns chegam ao rank de Herói, quem será o primeiro?!",
          placement: "bottom"
        },
        {
          element: "div.points",
          title: "Rank pessoal",
          content: "Seus pontos estão aqui, na medida que você for cadastrando novos pontos, sua pontuação irá aumentar automaticamente.",
          placement: "bottom"
        },
        {
          element: "div.general-rank",
          title: "Rank geral",
          content: "Aqui temos o rank com toda a galera! Toda vez que você entrar na dashboard, você poderá ver o rank de cada um atualizado.",
          placement: "top"
        },
        {
          element: "a.add-points",
          title: "Adicionando pontos",
          content: "Ao clicar neste botão, uma janela irá abrir para você preencher os dados do seu ponto. Já clicamos para você ;)",
          placement: "top",
          onShown: function() {
            $("a.add-points").trigger("click");
          }
        },
        {
          element: "div.modal-body",
          title: "Adicionando pontos",
          content: "Você vai se familiarizar com esta tela, pois aqui é onde você irá informar o que você fez no seu dia a dia.",
          placement: "bottom"
        },
        {
          element: ".tour-one",
          title: "Adicionando pontos",
          content: "É bem simples, basta apenas marcar o que você fez no dia",
          placement: "bottom",
          onShown: function() {
            $("label.tour-one").trigger("click");
          }
        },
        {
          element: ".tour-two",
          title: "Adicionando pontos",
          content: "Quando houverem desafios surpresas, ao marcar esta opção, irá aparecer uma caixa, a onde você poderá informar a quantidade dos pontos do desafio surpresa. Já clicamos para você ver ^^",
          placement: "bottom",
          onShown: function() {
            $("label.tour-two").trigger("click");
            $("div.surprise-points").slideToggle();
          }
        },
        {
          element: "#point_punctuation_date",
          title: "Adicionando pontos",
          content: "Você pode escolher as datas para incluir pontos, caso por algum motivo não pode cadastrar no dia",
          placement: "bottom"
        },
        {
          element: "#point_obs",
          title: "Adicionando pontos",
          content: "Este campo não é obrigatório, mas fique a vontade para preencher alguma atividade extra.",
          placement: "bottom"
        },
        {
          element: "input.btn.btn-raised.btn-success.ink-reaction",
          title: "Adicionando pontos",
          content: "Ao preencher todos os campos, basta clicar neste botão, e o cadastro estará feito!",
          placement: "bottom",
          onHidden: function() {
            $("button.btn.btn-raised.btn-info.ink-reaction").trigger("click");
          }
        },
        {
          element: "div.participant-points",
          title: "Verificando os pontos",
          content: "Aqui, você poderá ver a lista de todos os pontos que você cadastrou. Também é possível atualizar um ponto cadastrado, ou até remover.",
          placement: "top"
        },
        {
          element: "div.participant-points",
          title: "Você está pronto! Yay!!",
          content: "Agora você está pronto para mitar neste desafio! Boa sorte!!",
          placement: "top"
        }
      ],
      template: "<div class='card popover tour'>" +
                  "<div class='arrow'></div>" +
                  "<div class='card-head card-head-xs style-primary'>" +
                    "<header class='popover-title'></header>" +
                  "</div>" +
                  "<div class='card-body popover-content'>" +
                  "</div>" +
                  "<div class='popover-navigation'>" +
                    "<button class='btn btn-info' data-role='prev'>« " + I18n.t('actions.previous') + "</button>" +
                    "<span data-role='separator'>     </span>" +
                    "<button class='btn btn-info' data-role='next'>" + I18n.t('actions.next') + " »</button>" +
                    "<button class='btn btn-success' data-role='end'>" + I18n.t('actions.end') + "</button>" +
                  "</div>" +
                "</div>",
      onEnd: function (tour) {
        $(".tour-dashboard").remove();
      }
    });

    tour.init();
    tour.start();
  }
});
