$(document).on('turbolinks:load', function() {
  if ($("div.general-rank").length > 0) {

    var tour = new Tour({
      name: "dashboard",
      steps: [
        {
          element: "div.rank",
          title: "Challenge Rank",
          content: "Very well! This is the rank of the challenge, the higher your score, the higher your rank. Everyone starts like "+ I18n.t('activerecord.attributes.user.ranks.moises') +", but only a few get the "+ I18n.t('activerecord.attributes.user.ranks.hero') +" rank, who will be the first?!",
          placement: "bottom"
        },
        {
          element: "div.points",
          title: "Personal rank",
          content: "Your points are here, as you register for new points, your score will increase automatically.",
          placement: "bottom"
        },
        {
          element: "div.general-rank",
          title: "Overall rank",
          content: "Here we have the rank with all the guys! Every time you enter in the dashboard, you can see the overall rank updated.",
          placement: "top"
        },
        {
          element: "a.add-points",
          title: "Adding points",
          content: "By clicking on this button, a window will open for you to fill in the data of your point. We already clicked for you ;)",
          placement: "top",
          onShown: function() {
            $("a.add-points").trigger("click");
          }
        },
        {
          element: "div.modal-body",
          title: "Adding points",
          content: "You will get used with this page because here is where you will inform what you did day by day.",
          placement: "bottom"
        },
        {
          element: ".tour-one",
          title: "Adding points",
          content: "It's quite simple, just check what you did in the day",
          placement: "bottom",
          onShown: function() {
            $("label.tour-one").trigger("click");
          }
        },
        {
          element: ".tour-two",
          title: "Adding points",
          content: "When there is a surprise challenges, check this option and a box will appear. Here you can inform the amount of points of the surprise challenge. We already clicked for you to see ^^",
          placement: "bottom",
          onShown: function() {
            $("label.tour-two").trigger("click");
            $("div.surprise-points").slideToggle();
          }
        },
        {
          element: "#point_punctuation_date",
          title: "Adding points",
          content: "You can choose the dates to include points, if for some reason you cannot register on the same day",
          placement: "bottom"
        },
        {
          element: "#point_obs",
          title: "Adding points",
          content: "This field is not required, but feel free to fill in some extra activity.",
          placement: "bottom"
        },
        {
          element: "input.btn.btn-raised.btn-success.ink-reaction",
          title: "Adding points",
          content: "When filling in all the fields, just click on this button, and the registration will be done!",
          placement: "bottom",
          onHidden: function() {
            $("button.btn.btn-raised.btn-info.ink-reaction").trigger("click");
          }
        },
        {
          element: "div.participant-points",
          title: "Checking points",
          content: "Here you can see the list of all the points that you have registered. You can also update a registered point, or even remove it.",
          placement: "top"
        },
        {
          element: "div.participant-points",
          title: "You are ready! Yay!!",
          content: "Now you're ready to take on this challenge! Good luck!!",
          placement: "top"
        }
      ],
      template: "<div class='card popover tour'>" +
                  "<div class='arrow'></div>" +
                  "<div class='card-head card-head-xs style-primary'>" +
                    "<header class='popover-title'></header>" +
                  "</div>" +
                  "<div class='card-body popover-content'>" +
                  "</div>" +
                  "<div class='popover-navigation'>" +
                    "<button class='btn btn-info' data-role='prev'>« " + I18n.t('actions.previous') + "</button>" +
                    "<span data-role='separator'>     </span>" +
                    "<button class='btn btn-info' data-role='next'>" + I18n.t('actions.next') + " »</button>" +
                    "<button class='btn btn-success' data-role='end'>" + I18n.t('actions.end') + "</button>" +
                  "</div>" +
                "</div>",
      onEnd: function (tour) {
        $(".tour-dashboard").remove();
      }
    });

    tour.init();
    tour.start();
  }
});
