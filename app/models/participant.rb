class Participant < User
  has_and_belongs_to_many :challenges

  devise :confirmable

  before_validation :set_random_password

  private
    def set_random_password
      self.password = SecureRandom.hex(4) if self.password.blank?
    end
end
