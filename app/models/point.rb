class Point < ApplicationRecord
  belongs_to :user
  belongs_to :challenge

  validates :user, :challenge, :punctuation_date, presence: true
  validates :punctuation_date, uniqueness: {scope: :user_id, message: I18n.t('activerecord.errors.models.point.punctuation_date')}
  validates_with Validators::PointValidator

  after_save :check_user_rank

  def sum_points
    train + food_plan + coaching + sleep + surprise
  end

  private
    def check_user_rank
      new_rank = ranks(user.total_points)

      if new_rank > User.ranks[user.rank]
        user.update_rank(new_rank)
      elsif new_rank != User.ranks[user.rank]
        user.update_attribute(:rank, new_rank)
      end
    end

    def ranks(points)
      case points
      when 0..14
        return User.ranks[:moises]
      when 15..29
        return User.ranks[:trainee]
      when 30..44
        return User.ranks[:adapted]
      when 45..59
        return User.ranks[:scaled]
      when 60..89
        return User.ranks[:rx]
      when 90..119
        return User.ranks[:myth]
      when 120..160
        return User.ranks[:hero]
      end
    end
end
