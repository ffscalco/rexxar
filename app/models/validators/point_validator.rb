module Validators
  class PointValidator < ActiveModel::Validator
    def validate(record)
      unless record.sum_points > 0
        record.errors[:base] << I18n.t('activerecord.errors.models.point.no_points')
      end

      unless record.challenge.nil?
        unless record.punctuation_date.between?(record.challenge.start_date, record.challenge.end_date)
          record.errors[:punctuation_date] << "#{I18n.t('activerecord.errors.models.point.out_of_range')} (#{I18n.l(record.challenge.start_date)} - #{I18n.l(record.challenge.end_date)})"
        end
      end
    end
  end
end
