class Challenge < ApplicationRecord
  has_and_belongs_to_many :participants

  accepts_nested_attributes_for :participants, reject_if: :all_blank, allow_destroy: true

  validates :name, :start_date, :end_date, presence: true
end
