class User < ApplicationRecord
  devise :database_authenticatable, :validatable, :trackable,
    :recoverable
  has_many :points, dependent: :destroy

  validates :name, presence: true

  scope :admins, ->(admin) { where(admin: admin) }

  enum rank: {moises: 0, trainee: 1, adapted: 2, scaled: 3, rx: 4, myth: 5, hero: 6}

  def total_points
    points.map{|p| p.sum_points}.sum
  end

  def update_rank(new_rank)
    self.rank = new_rank
    self.levelup = true

    save
  end
end
